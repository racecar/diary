#!python3
import poplib, time, os, json
from email import parser

def connect(server, username, password):
    conn = poplib.POP3_SSL(server)
    conn.user(username)
    conn.pass_(password)
    return conn

with open("config.json") as f:
    config = json.load(f)

#connect to the pop server with the supplied credentials
conn = connect(config["pop_server"], config["pop_username"], config["pop_password"])
entries = []

for i in range(conn.stat()[0], 0, -1):
    #retrieve only headers for message and parse them
    msg = parser.BytesHeaderParser().parsebytes(b"\n".join(conn.top(i, 0)[1]))
    #check if message is to myself and has the case-insensitive subject "diary"
    if msg["subject"].lower() == "diary" and all(config["pop_username"] in msg[j] for j in ("to","from")):
        #retrieve full email and parse it into an EmailMessage
        msg = parser.BytesParser().parsebytes(b"\n".join(conn.retr(i)[1]))
        body = b""
        #get only the text body
        if msg.is_multipart():
            for part in msg.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get("Content-Disposition"))
                if ctype == "text/plain" and "attachment" not in cdispo:
                    body = part.get_payload(decode=True)
                    break
        else:
            body = msg.get_payload(decode=True)
        #format the time so it looks pretty
        date = time.strptime(msg["date"], "%a, %d %b %Y %H:%M:%S %z")
        date = time.strftime("%m/%d/%y", date)
        entries.append(date + ": " + body.decode().replace("\n", "") + "\n")

conn.quit()

if not os.path.exists("./out/"):
    os.mkdir("./out/")

with open("out/log.txt", "w", encoding="utf-8") as f:
    f.write("".join(entries))
