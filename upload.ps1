if (-not (test-path "$(get-date -f MMddyyyy).date")) {
    del *.date
    out-file "$(get-date -f MMddyyyy).date"
} else {
    exit
}
$config = type .\config.json | ConvertFrom-JSON 

py .\diary.py
py .\gen_index.py
cd .\out
$f = @("index.html", "log.txt", "feed.rss")
$f += ls -filter "page*" | select -expand Name
foreach ($i in $f) {
    curl -ks -H ("Authorization: Bearer {0}" -f $config.api_token) -F ("{0}=@{0}" -f $i) "https://neocities.org/api/upload"
}
cd ..\
