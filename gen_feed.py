import time
FEED_BASE = """\
<?xml version="1.0"?>
<rss version="2.0">
    <channel>
        <title>quick snippets</title>
        <link>https://f40c40.com</link>
        <description>A really quick overview of my day.</description>
{}
    </channel>
</rss>"""
FEED_ITEM = """\
        <item>
            <title>{}</title>
            <description><![CDATA[{}]]></description>
            <link>https://f40c40.com</link>
            <pubDate>{}</pubDate>
            <author>quokka</author>
        </item>"""
def fill_item(title, content, pubdate):
    pubdate = time.strftime("%a, %d %b %Y 00:00:00 +0000",time.strptime(pubdate,"%m/%d/%y"))
    return FEED_ITEM.format(title,content,pubdate)

def lenient_slice(s,start,pend):
    if pend >= len(s): return s
    return s[start:pend]
def gen_feed(posts):
    items = ""
    for i in range(10,-1,-1):
        if i < len(posts):
            date = posts[i][:8]
            content = posts[i][10:]
            title = lenient_slice(posts[i],0,32) + "..."
            items += fill_item(title,content,date) + "\n"
    return FEED_BASE.format(items)
