# diary

A weird thing that I use to journal by email.

To use this just rename `config.json.example` to `config.json`. You'll need to fill out all your login info for your email as well as your Neocities API key.
