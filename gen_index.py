#!python3
import gen_feed

OUT_TEMPLATE = """\
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <title>quick snippets</title>
 <meta name="viewport" content="width=device-width,initial-scale=1">
 <link rel="alternate" type="application/rss+xml" href="https://f40c40.com/feed.rss">
 <link rel="stylesheet" href="style.css">
</head>
<body>
<nav><ul id="nav">
<li><a rel="alternate" type="application/rss+xml" href="https://f40c40.com/feed.rss" class="feed">RSS feed</a></li>\
<li><a href="/other.html">Other stuff</a></li>
{}
</ul></nav>
<h1>Quick Snippets</h1>
<p>A really quick overview of my day.</p>
{}
</body>
</html>"""
def page_to_name(page):
    if page == 1: return "index.html"
    return "page" + str(page) + ".html"
def page_link(cur, page):
    if page-cur > 0:
        t = '<li><a href="{}">Page {} >></a></li>'
    else:
        t = '<li><a href="{}"><< Page {}</a></li>'
    return t.format(page_to_name(page), str(page))
f = open("out/log.txt", encoding="utf-8")
log_lines = f.readlines()
f.close()
p = 1
for i in range(0,len(log_lines),12):
    out = "<p>"+("</p><p>".join(log_lines[i:i+12]))+"</p>"
    page_nav = ""
    if len(log_lines) > 12:
        if p > 1:
            page_nav += page_link(p, p-1)
        if p+1 <= len(log_lines)//12+1:
            page_nav += page_link(p, p+1)
    f = open("out/" + page_to_name(p), "w", encoding="utf-8")
    f.write(OUT_TEMPLATE.format(page_nav, out).replace("\n",""))
    f.close()
    p += 1

with open("out/feed.rss", "w", encoding="utf-8") as f:
    f.write(gen_feed.gen_feed(log_lines))
